#include "AServer.h"
#include "Server.h"
#include <memory>
#include <thread>
#include <string>
#include <iostream>

void dialog( std::shared_ptr<AServer > server)
{
    do
    {
        std::cout<<"To close programm enter exit.\n";
        std::string answer;
        std::cin >> answer;
        if(answer.compare("exit") == 0)
        {
           server->stop();
           return;
        }
        else
           std::cout<<"Error read.Enter only exit !\n";
    }
    while(true);
}

int main ()
{
    std::shared_ptr<AServer > server(new Server());
    std::thread thrDialog(dialog,server);
    server->start(6000);
    thrDialog.join();
    return 0;
}


