#ifndef ASERVER_H
#define ASERVER_H

class AServer
{
public:
    virtual int start(unsigned int portNumber) = 0;
    virtual void stop() = 0;
    virtual bool isRunning() = 0;
    virtual bool isSocketOpen() = 0;
    virtual ~AServer() = 0;
};

#endif // ASERVER_H
