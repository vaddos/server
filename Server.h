#ifndef SERVER_H
#define SERVER_H
#include "AServer.h"
#include <netinet/in.h>
#include <map>

class Server : public AServer
{
    const unsigned int maxPortNumber;
    const unsigned int maxConnectionNumber;
    struct timeval timeout;
    bool running;
public:
    typedef int connection_desc;
    explicit Server(int maxConNumb = 10);
    virtual int start(unsigned int portNumber);
    virtual void stop();
    virtual bool isRunning();
    virtual bool isSocketOpen();
    ~Server();
protected:
    bool openSocket();
    bool openPort(unsigned int portNumber);
    int beginCommunication();
    bool checkForNewConnection(const fd_set &readset);
    void checkForNewData(fd_set &readset);
    bool checkForNewWork(fd_set &readset);
    void clearOldSet(fd_set &readset);
    bool findCLientInBase(const in_addr &addr,connection_desc &desc);
private:
    std::map<connection_desc, sockaddr_in> clients;
    typedef std::pair <connection_desc, sockaddr_in> descAddrPair;
    connection_desc servSockDesc;
    Server(const Server&);
    Server & operator = (const Server &s);

};

#endif // SERVER_H
