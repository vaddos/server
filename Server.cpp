#include "Server.h"
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <assert.h>
#include <arpa/inet.h>

Server::Server(int maxConNumb):
    maxPortNumber(65536),
    maxConnectionNumber(maxConNumb),
    timeout{15,0},
    running(false),
    servSockDesc(0)
{

}

bool Server::openSocket()
{
    servSockDesc = socket(AF_INET, SOCK_STREAM, 0);                                    // Create socket of domain - Internet (IP) address, type - Stream based (TCP) and protocol unspecified
                                                                                       // since it is only useful when underlying stack allows more than one protocol and we are choosing one.
                                                                                       // 0 means choose the default protocol.
    if(servSockDesc < 0)                                                               // A valid descriptor is always a positive value
    {
        std::cerr <<("Failed creating socket\n");
        return false;
    }
    fcntl(servSockDesc, F_SETFL, O_NONBLOCK);                                          //Эта операция превращает сокет в неблокирующий. Вызов любой функции с таким сокетом будет возвращать управление немедленно.
    return true;
}

bool Server::openPort(unsigned int portNumber)
{
    assert(servSockDesc > 0) ;
    if(portNumber > maxPortNumber)
    {
        std::cerr << "port number more then max/n";
        return false;
    }
    struct sockaddr_in serv_addr;
    bzero((char *)&serv_addr, sizeof(serv_addr));                                       // Initialize the server address struct to zero
    serv_addr.sin_family = AF_INET;                                                     // Fill server's address family
    serv_addr.sin_addr.s_addr = INADDR_ANY;                                             // Server should allow connections from any ip address
    serv_addr.sin_port = htons(portNumber);                                             // 16 bit port number on which server servSockDescs
                                                                                        // The function htons (host to network short) ensures that an integer is interpretted
                                                                                        // correctly (whether little endian or big endian) even if client and server have different architectures
    if (bind(servSockDesc, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)       // Attach the server socket to a port. This is required only for server since we enforce
    {                                                                                   // that it does not select a port randomly on it's own, rather it uses the port specified
        std::cerr << ("Failed to bind\n");                                              // in serv_addr struct.
        return false;
    }
    listen(servSockDesc, maxConnectionNumber);
    return true;
}


int Server::start(unsigned int portNumber)
{
    if (!openSocket())
        return 1;
    if (!openPort(portNumber))
        return 2;
    return beginCommunication();
}

int Server::beginCommunication()
{
   running = true;
   while(running)
   {
       fd_set readset;
       clearOldSet(readset);

       if(!checkForNewWork(readset))
           return 3;
       if(running)
       {
        checkForNewConnection(readset);
        checkForNewData(readset);
       }
   }
   return 0;
}

void Server::clearOldSet(fd_set &readset)
{
    FD_ZERO(&readset);                                                          // FD_ZERO(fd_set *set) - очищает множество set
    FD_SET(servSockDesc, &readset);                                             // add servSockDesc to readset
    for (auto client : clients)
        FD_SET(client.first, &readset);
}

bool Server::checkForNewWork(fd_set &readset)
{
    assert(servSockDesc > 0) ;                                                  //int select(int n, fd_set *readfds, fd_set *writefds,fd_set *exceptfds, struct timeval *timeout);
    int max;                                                                    //Функция select работает с тремя множествами дескрипторов, каждое из которых имеет тип fd_set.
    if(!clients.empty())                                                       //В множество readfds записываются дескрипторы сокетов, из которых нам требуется читать данные
    {                                                                           //(слушающие сокеты добавляются в это же множество). Множество writefds должно содержать дескрипторы сокетов,
        std::map<connection_desc, sockaddr_in>::iterator iter(clients.end());
        iter--;
        descAddrPair desc(*iter);                                               //в которые мы собираемся писать, а exceptfds - дескрипторы сокетов, которые нужно контролировать на возникновение ошибки.
        max = std::max(servSockDesc, desc.first);                               //Если какое-то множество вас не интересуют, вы можете передать вместо указателя на него NULL.
    }                                                                           //Что касается других параметров, в n нужно записать максимальное значение дескриптора по всем множествам плюс единица,
    else                                                                        //а в timeout - величину таймаута.
        max = servSockDesc;                                                     //Если хотя бы один сокет готов к выполнению заданной операции, select возвращает ненулевое значение, а все дескрипторы,
    if(select(max+1, &readset, NULL, NULL, &timeout) < 0)                       //которые привели к "срабатыванию" функции, записываются в соответствующие множества. Это позволяет нам проанализировать
    {                                                                           //содержащиеся в множествах дескрипторы и выполнить над ними необходимые действия. Если сработал таймаут, select возвращает ноль,
        std::cerr <<"select error\n";                                           //а в случае ошибки -1. Расширенный код записывается в errno.
        return false;
    }
    return true;
}

bool Server::checkForNewConnection(const fd_set &readset)
{
    assert(servSockDesc > 0) ;
    struct sockaddr_in client_addr;
    unsigned int size = sizeof(client_addr);
    if(FD_ISSET(servSockDesc, &readset))                                         //- проверяет, содержится ли дескриптор servSockDesc в множестве readset
    {                                                                            // Поступил новый запрос на соединение, используем accept
        descAddrPair desc;
        desc.first = accept(servSockDesc, (struct sockaddr *)&client_addr, &size);
        if(desc.first < 0)
        {
             std::cout << ("accept");
             return false;
        }
        desc.second = client_addr;
        fcntl(desc.first, F_SETFL, O_NONBLOCK);
        clients.insert(desc);
    }
    return true;
}

bool processingData(const std::string &buf, in_addr &inp,std::string &mesItself)
{
    const unsigned int maxPosOfBackspaceInStr = 15;
    std::string t_mes(buf);
    std::size_t pos = t_mes.find(" ");
    if (pos == std::string::npos || pos > maxPosOfBackspaceInStr)
        return false;
    std::string ipAddr(t_mes.substr(0,pos));
    inp.s_addr = 0;
    bool correctAddr = inet_aton(ipAddr.c_str(),&inp) != 0 ;            //inet_aton() returns non-zero if the address is a valid one, and it returns zero if the address is invalid.
    if(!correctAddr)
        return correctAddr;
    mesItself = t_mes.substr(pos + 1 , t_mes.length());
    return true;
}

int sendall(Server::connection_desc desc, const char *buf, int len, int flags)
{
    int total (0);
    int curSend(0);
    while(total < len)
    {
        curSend = send(desc, buf+total, len-total, flags);
        if(curSend == -1)
            break;
        total += curSend;
    }
    return (curSend == -1 ? -1 : total);
}

bool Server::findCLientInBase(const in_addr &addr,connection_desc &desc)
{
    for (auto client : clients)
    {
        if (client.second.sin_addr.s_addr == addr.s_addr)
        {
            desc = client.first;
            return true;
        }
    }
    return false;
}
void Server::checkForNewData(fd_set &readset)
{
    assert(servSockDesc > 0) ;
    const int bufSize = 1024;
    char buf[bufSize];
    int bytes_read;
    for (auto client : clients)
    {
        if(FD_ISSET(client.first, &readset))                            // Поступили данные от клиента, читаем их
        {
            bytes_read = recv(client.first, buf, bufSize, 0);
            if(bytes_read <= 0)                                         // Соединение разорвано, удаляем сокет из множества
            {
                close(client.first);
                clients.erase(client.first);
                continue;
            }

            std::string readStr(buf,bytes_read);
            in_addr addrToSend;
            std::string mesIself;
            if(processingData(readStr,addrToSend,mesIself))
            {
                connection_desc desc = 0;
                if(findCLientInBase(addrToSend,desc))                                                                           //have this addr
                {
                    std::string goodMes(">Message from : " + std::string(inet_ntoa(client.second.sin_addr)) + " - ");
                    goodMes.append(mesIself);
                    send(desc, goodMes.c_str(), goodMes.length(), 0);
                }
                else                                                    // Отправляем данные обратно клиенту
                {
                    std::string error("Don't have this user - " +  std::string(inet_ntoa(addrToSend)) + "\n");
                    sendall(client.first, error.c_str(), error.length(), 0);
                }
            }
            else                                                        // Отправляем данные обратно клиенту
            {
                std::string error("Bad message - ");
                error.append(buf);
                sendall(client.first, error.c_str(), error.length(), 0);
            }
        }
    }
}

bool Server::isRunning()
{
    return running;
}

bool Server::isSocketOpen()
{
    return servSockDesc > 0;
}

void Server::stop()
{
    running = false;
    if(!clients.empty())
    {
        for (auto client : clients)                                         // Program should always close all sockets (the connected one as well as the listening one)
            close(client.first);                                            // as soon as it is done processing with it
        clients.clear();
    }
    if(servSockDesc)
    {
        close(servSockDesc);
        servSockDesc = 0;
    }
}

Server::~Server()
{
    stop();
}
