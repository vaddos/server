
QMAKE_CXXFLAGS += -std=c++0x

HEADERS += \
    Server.h \
    AServer.h

SOURCES += \
    Server.cpp \
    main.cpp \
    AServer.cpp

CONFIG   += console
